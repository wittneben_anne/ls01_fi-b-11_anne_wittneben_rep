
import java.util.Scanner; // Import der Klasse Scanner 

public class Eingabe {

	public static void main(String[] args) {
		
		/* A2.2 Konsoleneingabe Aufgabe 2
		Neues Scanner-Objekt eingabe erstellen*/
		
		Scanner eingabe = new Scanner(System.in); 
		
		//Nutzer begr��en und nach Namen und Alter fragen
				
		System.out.println("Guten Tag!\n");
		System.out.println("Wie hei�en Sie?");
		
		String name ="";
		
		name = eingabe.next();
				
		//Nutzer nach Alter fragen
		
		System.out.println("Danke! \nWie alt sind Sie?");
		
		int alter;
		
		alter = eingabe.nextInt();
				
				
		//Beide Variabeln ausgeben
		
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter + " Jahre");
	
		eingabe.close();
				
	}

}
