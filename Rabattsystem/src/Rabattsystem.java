import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		
		double ermaeßigterBestellwert;
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie Ihren Bestellwert in Euro ein.");
		
		double bestellwert = myScanner.nextFloat();
		
		if (bestellwert >= 0 && bestellwert <= 100) {
			
			ermaeßigterBestellwert = 0.9 * bestellwert;
		}
		
		else if (bestellwert <= 500) {
			
			ermaeßigterBestellwert = 0.85 * bestellwert;			
		}
		
		else {
			
			ermaeßigterBestellwert = 0.8 * bestellwert;
		}
		
		double bestellwertInclMwst = 1.19 * ermaeßigterBestellwert;
		
		System.out.printf("Ihr ermäßigter Bestellwert incl. MwSt. beträgt %.2f Euro.", bestellwertInclMwst);
		
		myScanner.close();
	}

}
