import java.util.Scanner;

public class mittelwert {

	public static void main(String[] args) {
		
		double m;
		double summe = 0;
		double zahl;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert mehrerer Zahlen.");
		System.out.println("Von wie vielen Werten m�chten Sie den Mittelwert berechnen?");
		int anzahl = myScanner.nextInt();
		
		for (int i = 1; i <= anzahl; i++) {
				
			zahl = eingabe(myScanner, "Bitte geben Sie die " + i + ". Zahl ein: ");
		
			summe += zahl;
		}
		
		m = mittelwertBerechnung(summe, anzahl);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double gesamt, double nummer) {
		double m = gesamt/ nummer;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}
