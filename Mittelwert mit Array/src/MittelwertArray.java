import java.util.Scanner;

public class MittelwertArray {

	public static void main(String[] args) {
		
		double summe = 0.0;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert mehrerer Zahlen.");
		// Anzahl der Werte
		System.out.println("Von wie vielen Werten m�chten Sie den Mittelwert berechnen?");
		int anzahl = myScanner.nextInt();
		double zahlen [] = new double [anzahl];
		
		// Werte festlegen
		for (int i = 0; i < anzahl; i++) {	
			int a = i + 1;
			System.out.println("Bitte geben Sie die " + a + ". Zahl ein: ");
			zahlen [i] = myScanner.nextDouble();
		}
		
		//Summe berechnen
		for (double tmp : zahlen) {
			summe += tmp;
		}
		
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + summe/anzahl);
		
		myScanner.close();
		
	}
}

