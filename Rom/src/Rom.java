import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine einstellige r�mische Ziffer ein.");
		char ziffer = myScanner.next().charAt(0);
		
		switch (ziffer) {
		
		case 'I':
			System.out.println(1);
			break;
			
		case 'V':
			System.out.println(5);
			break;
			
		case 'X':
			System.out.println(10);
			break;
			
		case 'L':
			System.out.println(50);
			break;

		case 'C':
			System.out.println(100);
			break;		
			
		case 'D':
			System.out.println(500);
			break;
			
		case 'M':
			System.out.println(1000);
			break;
			
		default:
			System.out.println("Das eingegebene Zeichen ist keine r�mische Ziffer.");
			
		myScanner.close();
		}

	}

}
