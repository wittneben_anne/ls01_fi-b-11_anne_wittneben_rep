import java.util.Scanner;

public class Fahrkartenarray {
	// main-Methode
		public static void main(String[] args)
	    {
	       Scanner tastatur = new Scanner(System.in);
	      
	       while (true) {
	    	   float zuZahlenderBetrag = fahrkartenbestellungErfassen (tastatur); 
	    	   float rueckgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag, tastatur);
	    	   fahrkartenAusgeben();
	    	   rueckgeldAusgeben(rueckgabebetrag);
	       }         
	    }
	       
		// Methode fahrkartenbestellungErfassen
		
		public static float fahrkartenbestellungErfassen (Scanner scanf) {	
			float zuZahlen = 0.0f;
			byte wahl;
			float [] preis = {2.9f, 3.3f, 3.6f, 1.9f, 8.6f, 9.0f, 9.6f, 23.5f, 24.3f, 24.9f};
			String [] bezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
			
			byte anzahl = 0;
			System.out.println("Fahrkartenbestellvorgang:");
			for (int i = 0; i < 26; i++) {
				System.out.print("=");
			}
			System.out.println("\n\n");
			
			
			
			// Ticketwahl
			do {
				System.out.println("W�hlen Sie ihre Wunschfahrkarte aus:");
				for (int i = 0; i < preis.length-1; i++) {
					int a = i + 1;
					System.out.printf("  " + bezeichnung[i] + " [" + "%.2f" + " Euro] (" + a + ")\n", preis[i] );
				}
				System.out.println("  Bezahlen (99)\n");
				
				System.out.print("Ihre Wahl: ");	
				wahl = scanf.nextByte();
				
				if (wahl > 0 && wahl < preis.length) {			
				 	// Abfrage Ticketanzahl
					System.out.print("Anzahl der Tickets: ");
					anzahl = scanf.nextByte();
					
					// Ticketgrenzen
					while (anzahl < 1 || anzahl > 10){
						System.out.println("\nW�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
						System.out.print("Anzahl der Tickets: ");
						anzahl = scanf.nextByte();
					}
					// Berechnung des zu zahlenden Betrags
					zuZahlen += (anzahl * preis[wahl-1]);
					System.out.printf("\nZwischensumme: %.2f �\n\n", zuZahlen);
				} else if (wahl == 99) {
					break;
				} else {
					System.out.println(" >Ung�ltige Eingabe. Bitte w�hlen Sie eine der nachfolgenden Optionen.<\n\n");
				}		
				
			} while (wahl != 99);
			return zuZahlen;
	    }
	    
	    
		// Methode fahrkartenBezahlen
		public static float fahrkartenBezahlen (float zuZahlen, Scanner scan) {
	       
			// Geldeinwurf
			float eingezahlt = 0.0f;
			while(eingezahlt < zuZahlen)
			{
				System.out.printf("Zu zahlen: %.2f EURO\n", zuZahlen - eingezahlt);
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				float eingeworfeneM�nze = scan.nextFloat();
				eingezahlt += eingeworfeneM�nze;
	    	   	}
	    	   
			// R�ckgeldberechnung
			float rueckgabe = eingezahlt - zuZahlen;
			return rueckgabe;
		}

		
		// Mehode fahrkartenAusgeben
		public static void fahrkartenAusgeben() {	  
	    	   
			System.out.println("\nFahrschein(e) werden ausgegeben");
			for (int i = 0; i < 8; i++)
			{
				System.out.print("=");
				int zeit = 250;
				warte(zeit);
			}
			
			System.out.println("\n\n");
		}
	       
		
		// Methode warte
		
		public static void warte(int millisekunde) {
			try {
				Thread.sleep(millisekunde);
			} 
			catch (InterruptedException e) {
			
				e.printStackTrace();
			}
		}
		// Methode rueckgeldAusgeben
		public static void rueckgeldAusgeben(float rueckgeld) {
	       
			if(roundRueckgeld(rueckgeld) > 0.00f)
			{
				System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO wird in folgenden M�nzen ausgezahlt:\n", rueckgeld);
				String euro = "EURO";
				String cent = "CENT";
				
				while(roundRueckgeld(rueckgeld) >= 2.0) // 2 EURO-M�nzen
				{
					int geld1 = 2;
					muenzeAusgeben(geld1, euro);
					rueckgeld -= 2.00;
				}
				while(roundRueckgeld(rueckgeld) >= 1.0) // 1 EURO-M�nzen
				{
					int geld2 = 1;
					muenzeAusgeben(geld2, euro);
					rueckgeld -= 1.0;
				}
				while(roundRueckgeld(rueckgeld) >= 0.5) // 50 CENT-M�nzen
				{
					int geld3 = 50;
					muenzeAusgeben(geld3, cent);
					rueckgeld -= 0.5;
	       		}           
				while(roundRueckgeld(rueckgeld) >= 0.2) // 20 CENT-M�nzen
				{
					int geld4 = 20;
					muenzeAusgeben(geld4, cent);
					rueckgeld -= 0.2;
	       		}
				while(roundRueckgeld(rueckgeld) >= 0.1) // 10 CENT-M�nzen
				{
					int geld5 = 10;
					muenzeAusgeben(geld5, cent);
					rueckgeld -= 0.1;
	       		}
				while(roundRueckgeld(rueckgeld) >= 0.05)// 5 CENT-M�nzen
				{
					int geld6 = 5;
					muenzeAusgeben(geld6, cent);
					rueckgeld -= 0.05; 
	       		}
			}
			System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"+
					"Wir w�nschen Ihnen eine gute Fahrt.\n\n");
		}
		
		// Methode roundRueckgeld
		public static float roundRueckgeld(float rueckgeld) {
			return Math.round(rueckgeld * 100) / 100.0f;
		}
		// Methode muenzeAusgeben
		
		public static void muenzeAusgeben(int betrag, String einheit) {
			
			System.out.println(betrag + " " + einheit);
		}
}    