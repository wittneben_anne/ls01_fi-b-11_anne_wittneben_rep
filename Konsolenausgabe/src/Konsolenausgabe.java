
public class Konsolenausgabe {

	public static void main(String[] args) {
//		System.out.println("Hallo\nwie geht es Dir?");
//		System.out.println("Hallo\twie geht es Dir?");
//		System.out.println("Hallo\rwie geht es Dir?");
//		System.out.println("Hallo\fwie geht es Dir?");
//		System.out.println("Hallo\bwie geht es Dir?");
//		System.out.println("Hallo\"wie geht es Dir?");
//		System.out.println("Hallo\'wie geht es Dir?");
//		System.out.println("Hallo\\wie geht es Dir?");
//		
//		String z = "Java-Programm"; 
//		System.out.printf( "\n|%s|\n", z );
//		System.out.printf( "|%10s|\n", z );
//		System.out.printf( "|%-20.4s|\n", z );
//		
//		int n = 123;
//		System.out.printf( "\n|%d|   |%d|\n" ,     n, -n);
//		System.out.printf( "|%-5d| |%6d|\n" ,     n, -n);
//		
//		double d = 1234.5678;
//		System.out.printf( "\n|%f| |%f|\n" ,         d, -d);  
//		System.out.printf( "|%.3f| |%.3f|\n" ,         d, -d); 
		
//		Aufgabe 1
		
		int i = 1;
		String klasse = "FI-B-11";
		
		System.out.print("\nIch bearbeite \"Aufgabe " + i + "\".\n");
		System.out.println("Ich besuche die Klasse " + klasse + ".");
		
//		Aufgabe 2
		
			
		System.out.printf("\n%7s\n", "*" );
		System.out.printf("%8s\n", "***" );
		System.out.printf("%9s\n", "*****" );
		System.out.printf("%10s\n", "*******" );
		System.out.printf("%11s\n", "*********" );
		System.out.printf("%12s\n", "***********" );
		System.out.printf("%13s\n", "*************" );
		System.out.printf("%8s\n", "***" );
		System.out.printf("%8s\n", "***" );

//		Aufgabe 3
		
		System.out.println("\nAufgabe 3\n");
		
		double z1 = 22.4234234;
		double z2 = 111.2222;
		double z3 = 4.0;
		double z4 = 1000000.551;
		double z5 = 97.34;
		
		System.out.printf("%.2f\n" , z1 );
		System.out.printf("%.2f\n" , z2 );
		System.out.printf("%.2f\n" , z3 );
		System.out.printf("%.2f\n" , z4 );
		System.out.printf("%.2f\n" , z5 );
			
	}

}
