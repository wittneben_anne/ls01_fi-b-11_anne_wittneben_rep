import java.util.Scanner;

public class Captain {

	// Attribute
	private String surname;
	private int captainYears;
	private double gehalt;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	private String name;
	
	Scanner myScan = new Scanner(System.in);
	// Konstruktoren

	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert.
	public Captain (String name, String surname) {
		this.surname = surname;
		this.name = name;		
	}
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	public Captain (String name, String surname, int captainYears, double gehalt) {
		this.surname = surname;
		this.name = name;	
		this.captainYears = captainYears;
		this.gehalt = gehalt;
	}

	// Verwaltungsmethoden
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if (gehalt >= 0.0) {
			this.gehalt = gehalt;
		}
		else {
			this.gehalt = 0.0;
			System.out.println("Ein negatives Gehalt ist nicht m�glich. Das Gehalt wurde auf 0.0 gesetzt.");
		}
		
	}
	
	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methode.
		return this.gehalt;
	}

	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
	public void setCaptainYears(int captainYears) {
		if (captainYears >= 0) {
			this.captainYears = captainYears;
		}
		else { 
			this.captainYears = 0;
			System.out.println("Jahre vor Christus sind nicht m�glich. Das Jahr wurde auf 0 gesetzt.");
		}
	}
	
	public int getCaptainYears() {
		return this.captainYears;
	}
	
	// Weitere Methoden
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname() {
		return surname + " " + name;
	}

	public String toString() {  // overriding the toString() method
		// TODO: 8. Implementieren Sie die toString() Methode.
		return surname + " " + name + " " + gehalt + " " + captainYears;
	}

}