import java.util.Scanner;

public class Treppe {

	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die Treppenhöhe ein: ");
		int h = myScanner.nextInt();
		System.out.println("Bitte geben Sie die Treppenbreite ein: ");
		int b = myScanner.nextInt();
		
		for (int i = 1; i <= h; i++) {
					
			for (int j = 0; j < (h*b)-(b*i); j++) {
				System.out.print(" ");
			}
		
			for (int j = 0; j < b*i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		myScanner.close();
				
	}

}
