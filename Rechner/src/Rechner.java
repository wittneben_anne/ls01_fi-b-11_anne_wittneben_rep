import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
    
    float zahl1 = 0.0f, zahl2 = 0.0f, summe = 0.0f;
    int  zahl3, zahl4, zahl5, zahl6, zahl7, zahl8;
    double zahldouble;
    long zahllong;
    byte zahlbyte;
    short zahlshort;
    boolean wahrheitswert;
    char buchstabe;
    
   
    String name = "";
    
    System.out.print("Bitte geben Sie eine Gleitkommazahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    zahl1 = myScanner.nextFloat();
     
    System.out.print("Bitte geben Sie eine zweite Gleitkommazahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    zahl2 = myScanner.nextFloat();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    summe = zahl1 + zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.println(zahl1 + " + " + zahl2 + " = " + summe); 
    
    //Namen/String eingeben und Begr��ung
    
    System.out.println("Bitte geben Sie Ihren Namen ein:");
    
    name = myScanner.next();
    
    System.out.println("Hallo " + name + "!");
    
    //Subtraktion zweier selbst eingegebener Zahlen
    
    System.out.print("\nBitte geben Sie eine ganze Zahl ein: ");    
    
    zahl3 = myScanner.nextInt();
    
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");    
    
    zahl4 = myScanner.nextInt();
    
    System.out.println("\nDas Ergebnis Ihrer Subtraktion ist:");
    
    System.out.println(zahl3 - zahl4);
    
    //Multiplikation zweier selbst eingegebener Zahlen
    
    System.out.print("\nBitte geben Sie noch eine ganze Zahl ein: ");    
    
    zahl5 = myScanner.nextInt();
    
    System.out.print("Bitte geben Sie eine weitere ganze Zahl ein: ");    
    
    zahl6 = myScanner.nextInt();
    
    System.out.println("\nDas Ergebnis Ihrer Multiplikation ist:");
    
    System.out.println(zahl5 * zahl6);
    
    //Division zweier selbst eingegebener Zahlen
    
    System.out.print("\nBitte geben Sie erneut eine ganze Zahl ein: ");    
    
    zahl7 = myScanner.nextInt();
    
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
    
    zahl8 = myScanner.nextInt();
    
    System.out.println("\nDas Ergebnis Ihrer Division ist:");
    
    System.out.println(zahl7 / zahl8);
   
    //Datentypen eingeben
    
    System.out.println("\nDouble:");
    zahldouble = myScanner.nextDouble();
    System.out.println("Double = " + zahldouble);		
    
    System.out.println("Byte:");
    zahlbyte = myScanner.nextByte();
    System.out.println("Byte = " + zahlbyte);
    
    System.out.println("Short:");
    zahlshort = myScanner.nextShort();
    System.out.println("Short = " + zahlshort);
    		
    System.out.println("Long:");
    zahllong = myScanner.nextLong();
    System.out.println("Long = " + zahllong);
    
    System.out.println("Boolean:");
    wahrheitswert = myScanner.nextBoolean();
    System.out.println("Boolean = " + wahrheitswert);
    
    System.out.println("Char:");
    buchstabe = myScanner.next(). charAt(0);
    System.out.println("Char = " + buchstabe);
    
    myScanner.close(); 
     
  }    
} 