import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesString("Was moechten Sie bestellen?", myScanner);
		int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);
		double preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);
		double bruttogesamtpreis = berechneGesamtbruttopreis(mwst, nettogesamtpreis);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		myScanner.close();
		
	}

	public static String liesString(String text, Scanner ms) {
		
		System.out.println(text);
		String name = ms.next();
		return name;
	}
	
	public static int liesInt(String text, Scanner ms) {
		
		System.out.println(text);
		int zahl = ms.nextInt();
		return zahl;
	}
	
	public static double liesDouble(String text, Scanner ms) {
		
		System.out.println(text);
		double netto = ms.nextDouble();
		return netto;
	}
	
	public static double berechneGesamtnettopreis(int anzahl1, double einzelpreis) {
		
		double nettogesamt = anzahl1 * einzelpreis;
		return nettogesamt;
	}
	
	public static double berechneGesamtbruttopreis(double steuer, double nettogesamt) {
		
		double bruttogesamt = nettogesamt * (1 + steuer / 100);
		return bruttogesamt;
		
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
	
}