import java.util.Scanner;

class FahrkartenEndlos {

	// main-Methode
	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       while  (true) {
    	   float zuZahlenderBetrag = fahrkartenbestellungErfassen (tastatur); 
    	   float rueckgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag, tastatur);
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben(rueckgabebetrag);
       }         
    }
       
	// Methode fahrkartenbestellungErfassen
	
	public static float fahrkartenbestellungErfassen (Scanner scanf) {	
		float einzelPreis = 0.0f;
		int ticketWahl = 0;
		byte anzahl;
		
		System.out.println("Fahrkartenbestellvorgang:");
		for (int i = 0; i < 26; i++) {
			System.out.print("=");
		}
		System.out.println("\n\n");
		
		// Ticketwahl
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
		
		while (ticketWahl !=1 && ticketWahl != 2 && ticketWahl != 3){
			System.out.print("Ihre Wahl: ");	
			ticketWahl = scanf.nextInt();
			if (ticketWahl == 1) { 
				einzelPreis = 2.90f;
			}
			else if (ticketWahl == 2) {
				einzelPreis = 8.60f;
			}
			else if (ticketWahl == 3) {
				einzelPreis = 23.5f;
			}
			else {
				System.out.println(">>falsche Eingabe<<");
			}
		}
     
		// Abfrage Ticketanzahl
		System.out.print("Anzahl der Tickets: ");
		anzahl = scanf.nextByte();
		// Ticketgrenzen
		while (anzahl < 1 || anzahl > 10){
			System.out.println("\nW�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			System.out.print("Anzahl der Tickets: ");
			anzahl = scanf.nextByte();
		}
       
		// Berechnung des zu zahlenden Betrags
		float zuZahlen = anzahl * einzelPreis;
		return zuZahlen;
    }
    
    
	// Methode fahrkartenBezahlen
	public static float fahrkartenBezahlen (float zuZahlen, Scanner scan) {
       
		// Geldeinwurf
		float eingezahlt = 0.0f;
		while(eingezahlt < zuZahlen)
		{
			System.out.printf("Zu zahlen: %.2f EURO\n", zuZahlen - eingezahlt);
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			float eingeworfeneM�nze = scan.nextFloat();
			eingezahlt += eingeworfeneM�nze;
    	   	}
    	   
		// R�ckgeldberechnung
		float rueckgabe = eingezahlt - zuZahlen;
		return rueckgabe;
	}

	
	// Mehode fahrkartenAusgeben
	public static void fahrkartenAusgeben() {	  
    	   
		System.out.println("\nFahrschein(e) werden ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			int zeit = 250;
			warte(zeit);
		}
		
		System.out.println("\n\n");
	}
       
	
	// Methode warte
	
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} 
		catch (InterruptedException e) {
		
			e.printStackTrace();
		}
	}
	// Methode rueckgeldAusgeben
	public static void rueckgeldAusgeben(float rueckgeld) {
       
		if(roundRueckgeld(rueckgeld) > 0.00f)
		{
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO wird in folgenden M�nzen ausgezahlt:\n", rueckgeld);
			String euro = "EURO";
			String cent = "CENT";
			
			while(roundRueckgeld(rueckgeld) >= 2.0) // 2 EURO-M�nzen
			{
				int geld1 = 2;
				muenzeAusgeben(geld1, euro);
				rueckgeld -= 2.00;
			}
			while(roundRueckgeld(rueckgeld) >= 1.0) // 1 EURO-M�nzen
			{
				int geld2 = 1;
				muenzeAusgeben(geld2, euro);
				rueckgeld -= 1.0;
			}
			while(roundRueckgeld(rueckgeld) >= 0.5) // 50 CENT-M�nzen
			{
				int geld3 = 50;
				muenzeAusgeben(geld3, cent);
				rueckgeld -= 0.5;
       		}           
			while(roundRueckgeld(rueckgeld) >= 0.2) // 20 CENT-M�nzen
			{
				int geld4 = 20;
				muenzeAusgeben(geld4, cent);
				rueckgeld -= 0.2;
       		}
			while(roundRueckgeld(rueckgeld) >= 0.1) // 10 CENT-M�nzen
			{
				int geld5 = 10;
				muenzeAusgeben(geld5, cent);
				rueckgeld -= 0.1;
       		}
			while(roundRueckgeld(rueckgeld) >= 0.05)// 5 CENT-M�nzen
			{
				int geld6 = 5;
				muenzeAusgeben(geld6, cent);
				rueckgeld -= 0.05; 
       		}

		}


		System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir w�nschen Ihnen eine gute Fahrt.\n\n");
	}
	
	// Methode roundRueckgeld
	public static float roundRueckgeld(float rueckgeld) {
		return Math.round(rueckgeld * 100) / 100.0f;
	}
	
	// Methode muenzeAusgeben
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		
		System.out.println(betrag + " " + einheit);
	}

}    