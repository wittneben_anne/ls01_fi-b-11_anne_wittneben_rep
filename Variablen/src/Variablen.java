
	
		//package eingabeAusgabe;

		/** Variablen.java
		    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
		    @author
		    @version
		*/
		public class Variablen {
		  public static void main(String [] args){
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */

			byte zaehler;
			  
		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/

			zaehler = 25;
			System.out.println("Z�hler: " + zaehler);
			
		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */

			char menuepunkt;
			 
		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
			
			menuepunkt = 'C';
			System.out.println("Men�punkt: " + menuepunkt);

		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */
			
			long astro;

		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/

			astro = 299792458;
			System.out.println("Lichtgeschwindigkeit in Metern pro Sekunde: " + astro);
			
		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/

			byte mitglieder = 7;
			
		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

			System.out.println("Anzahl der Mitglieder: " + mitglieder);
			
		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/
			
			double elementarladung = 1.602176634e-19;
			System.out.println("Elektrische Elementarladung in Coulomb: " + elementarladung);
			
		    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */
			
			boolean zahlung;

		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/
			
			zahlung = true;
			System.out.println("Zahlung erfolgt: " + zahlung);

		  }//main
		}// Variablen





