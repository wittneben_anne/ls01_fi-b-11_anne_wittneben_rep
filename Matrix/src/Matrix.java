import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Matix");
		
		// Ziffer eingeben
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		
		int ziffer = scan.nextInt();
				
		int i = 0;
		int j = 0;
		
		// i gibt erste Stelle an & j gibt zweite Stelle an
		
		while (i < 10) {
			while (j < 10) {
				
				// Berechnung der Zahl von 0 bis 99
				int zahl = (10*i)+j;
				
				// Ausnahme, falls zahl = 0, da sonst zahl%0 == 0 ist
				if (i == 0 && j == 0) {
					System.out.print(" 0 ");
				}
				
				// Falls ziffer enthalten, zahl durch die ziffer restlos teilbar ist oder Quersumme gleich ziffer
				else if (i == ziffer || j == ziffer || zahl%ziffer == 0 || (i + j) == ziffer) {
					System.out.print(" * ");
				}
				
				// Sonst Zahl ausgeben
				else {
					// nur f�r Formatierung, da einstellige Zahlen weniger Platz brauchen
					if (i == 0) {
						System.out.print(" ");
					}
					
					System.out.print(zahl + " ");
				}
				j++;
			}
			// neue Zeile
			System.out.println();
			j = 0;
			i++;
		}
		
		scan.close();

	}

}
