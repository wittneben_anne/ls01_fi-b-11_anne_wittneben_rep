
public class Temperaturtabelle {

	public static void main(String[] args) {
	
		String s1 = "Fahrenheit";
		String s2 = "Celsius";		
				
		double f1 = -20;
		double f2 = -10;
		double f3 = 0;
		double f4 = 20;
		double f5 = 30;
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		System.out.printf( "%-12s" , s1 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10s\n" , s2 ); 
		
		System.out.println("-----------------------");
		
		System.out.printf( "%-12.2f" , f1 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c1 ); 
		
		System.out.printf( "%-12.2f" , f2 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c2 ); 
		
		System.out.printf( "%+-12.2f" , f3 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c3 ); 
		
		System.out.printf( "%+-12.2f" , f4 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c4 ); 
		
		System.out.printf( "%+-12.2f" , f5 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c5 ); 


	}

}
