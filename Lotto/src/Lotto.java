
public class Lotto {

	public static void main(String[] args) {
		
		byte [] lottozahlen = {3, 7, 12, 18, 37, 42};
		
		// Aufgabe 4a
		System.out.print("[ ");
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print(lottozahlen[i] + " ");
		}
		System.out.print("]\n");
		
		// Aufgabe 4b
		byte nummer1 = 12; 
		byte nummer2 = 13;
		
		test(nummer1, lottozahlen);
		test(nummer2, lottozahlen);
		
	}
	public static void test(byte zahl, byte [] lottozahlen) {
		boolean enthalten = false;
		for (byte tmp : lottozahlen) {
			if (tmp == zahl) {
				enthalten = true;
			}
		}
		if (enthalten == true) {
			System.out.println("Die Zahl " + zahl + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + zahl + " ist nicht in der Ziehung enthalten.");
	}
		

	}

}
