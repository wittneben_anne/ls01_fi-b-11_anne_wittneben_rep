
public class Wettlauf {

	public static void main(String[] args) {
		
		float a = 0.0f;
		float b = 250.0f;
		
		System.out.printf("Sprinter A | Sprinter B\n");
		do {
			a += 9.5f;
			b +=7.5f;
			
			System.out.printf("%8.1f m |", a);
			System.out.printf("%8.1f m \n", b);
				
		} while (a < 1000 && b < 1000);

	}

}
