
public class ArrayHelper {
	public static void main(String[] args) {
		
		int [] nummern = {1, 2, 3, 4, 5, 6, 7};
		System.out.println(convertArrayToString(nummern));
			}
	
	public static String convertArrayToString(int[] zahlen) {
		String ausgabe = "";
		for (int tmp : zahlen) {
			ausgabe += Integer.toString(tmp) + ", ";
		}
		return ausgabe;
	}

}
