import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ein Jahr ein. Geben Sie Jahre v. Chr. bitte als negative Zahlen ein.");
		
		int jahr = scan.nextInt();
		
		scan.close();
		
		// Einführung von Schaltjahren 45 v. Chr.
		if (jahr >= -45) {
			
			// Vor 1582 gilt nur Regel 1
			if (jahr < 1582) {
				if (jahr%4 == 0) {
					System.out.println(jahr + " ist ein Schaltjahr.");
				}
				else {
					System.out.println(jahr + " ist kein Schaltjahr.");
				}
			}
			
			//Nach 1582
			else {
				//Regel 1
				if ((jahr%4) == 0) {
					
					// Regel 2
					if ((jahr%100) == 0) {
						
						// Regel 3
						if ((jahr%400) == 0) {
						System.out.println(jahr + " ist ein Schaltjahr.");
						} else {
						System.out.println(jahr + " ist kein Schaltjahr.");
						}
					} else {
						System.out.println(jahr + " ist ein Schaltjahr.");
					}
					
				} else {
					System.out.println(jahr + " ist kein Schaltjahr.");
				}
			}
		
		// Keine Schaltjahre vor 45 v. Chr.
		} else {
			System.out.println(jahr + " ist kein Schaltjahr.");
		}
		
	}

}
