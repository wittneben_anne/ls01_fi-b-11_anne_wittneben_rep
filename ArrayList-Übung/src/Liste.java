import java.util.ArrayList;
import java.util.Scanner;

public class Liste {

	public static void main(String[] args) {
	
		Scanner myScanner = new Scanner(System.in);
		
		ArrayList<Integer> myList = new ArrayList <Integer>();
		
		for (int i = 0; i < 20; i++) {
			myList.add((int) (Math.random()*9 + 1));
		}
		
		System.out.println("Ausgabe: Liste mit 20 Zufallszahlen");
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("myList[%2d] : %d\n", i, myList.get(i));
		}
		
		System.out.println("Eine Zahl zwischen 1 und 9: ");
		int zahl = myScanner.nextInt();
		int anzahl = 0;
		
		for (int i = 0; i < myList.size(); i++) {
			if (myList.get(i) == zahl){
				anzahl++;
			}
		}
		
		System.out.println("\nDie Zahl " + zahl + " kommt " + anzahl + " mal in der Liste vor.\n");
		
		System.out.println("Die Zahl kommt an folgenden Indices in der Liste vor:");
		
		for (int i = 0; i < myList.size(); i++) {
			if (myList.get(i) == zahl){
				System.out.printf("myList[%2d] : %d\n", i, myList.get(i));
			}	
		}

		for (int i = myList.size()-1; i >= 0; i--) {
			if (myList.get(i) == zahl) {
				myList.remove(i);
			}
		}

		
		System.out.println("\nListe nach L�schung von " + zahl);
		for (int i = 0; i < myList.size(); i++) {
			System.out.printf("myList[%2d] : %d\n", i, myList.get(i));
		}		
		

		System.out.println("\nListe nach Einfuegen von 0 hinter jeder 5");
		for (int i = 0; i < myList.size(); i++) {
			if (myList.get(i) == 5){
				myList.set(i+1, 0);
			}
			System.out.printf("myList[%2d] : %d\n", i, myList.get(i));
		}
		myScanner.close();
	}

}
