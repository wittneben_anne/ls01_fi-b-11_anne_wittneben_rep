import java.util.ArrayList;
/**
 * 
 * @author Anne Wittneben
 *
 */
public class Raumschiff {

	// Attribute
	private int torpedoAnzahl;
	private int energieInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungInProzent;
	private int androidenAnzahl;
	private String name;
	public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktoren
	
	public Raumschiff() {
	}
	
	/**Konstruktor
	 * (Effekt.:) erzeuget vollparameteriesiertes Objekt der Klasse Raumschiff
	 * @param torpedoAnzahl Anzahl der geladenen Photonentorpedos des Raumschiffs 
	 * @param energieInProzent Energieversorgung des Raumschiffs in Prozent
	 * @param schildeInProzent Zustand der Schilde des Raumschiffs in Prozent
	 * @param huelleInProzent Zustand der H�lle des Raumschiffs in Prozent
	 * @param lebenserhaltungInProzent Zustand der Lebenserhaltungssysteme in Prozent
	 * @param androidenAnzahl Anzahl der vom Raumschiff verf�gbaren Reparaturandroiden
	 * @param name Name des Raummschiffs
	 */
	public Raumschiff(int torpedoAnzahl, int energieInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungInProzent, int androidenAnzahl, String name) {
		this.torpedoAnzahl = torpedoAnzahl;
		this.energieInProzent = energieInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.name = name;
	}

	// Verwaltungsmethoden
	public int getTorpedoAnzahl() {
		return this.torpedoAnzahl;
	}

	public void setTorpedoAnzahl(int torpedoAnzahl) {
		this.torpedoAnzahl = torpedoAnzahl;
	}


	public int getEnergieInProzent() {
		return this.energieInProzent;
	}

	public void setEnergieInProzent(int energieInProzent) {
		this.energieInProzent = energieInProzent;
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungInProzent() {
		return this.lebenserhaltungInProzent;
	}

	public void setLebenserhaltungInProzent(int lebenserhaltungInProzent) {
		this.lebenserhaltungInProzent = lebenserhaltungInProzent;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// andere Methoden
	/**Voraussetzung: Objekt der Klasse Ladung muss vorhanden sein
	 * (Effekt.:) f�gt ein Objekt der Klasse Landung dem Ladungsverzeichnis hinzu
	 * @param neueLadung eine neue Ladung wird in das Ladungsverzeichnis eingetragen, indem ein Objekt der Klasse Ladung der ArrayList ladungsverzeichnis hizugef�gt wird
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**Voraussetzung: Objekt der Klasse Raumschiff muss vorhanden sein
	 * (Effekt.:) ist die Energieversorgung kleiner als 50%, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
	 * Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle �Phaserkanone abgeschossen� gesendet. 
	 * Der Treffer wird vermerkt.
	 * @param ziel Objekt der Klasse Raumschiff, auf das geschossen wird.
	 */
	public void phaserKanoneSchiessen(Raumschiff ziel) {
		if (this.energieInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(ziel);
		}
	}

	/**Voraussetzung: Objekt der Klasse Raumschiff muss vorhanden sein
	 * (Effekt.:) Gibt es keine Torpedos, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
	 * Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet. 
	 * Au�erdem wird die Methode Treffer aufgerufen.
	 * @param ziel Objekt der Klasse Raumschiff, auf das geschossen wird.
	 */
	public void photonentorpedosSchiessen(Raumschiff ziel) {
		if (this.torpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.torpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen ");
			treffer(ziel);
		}
	}

	/**Voraussetzung: Wird von einer Methode in derselben Klasse aufgerufen.
	 * (Effekt.:) Die Nachricht "[Schiffsname] wurde getroffen!" wird in der Konsole ausgegeben. [Schiffsname] durch den Namen des Schiffes ersetzen.
	 * Die Schilde des getroffenen Raumschiffs werden um 50% geschw�cht. Ist anschlie�end die Schilde vollst�ndig zerst�rt worden sein, so wird der Zustand der H�lle und der Energieversorgung jeweils um 50% abgebaut.
	 * Sollte danach der Zustand der H�lle auf 0% absinken, so sind die Lebenserhaltungssysteme vollst�ndig zerst�rt und es wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
	 * @param ziel Objekt der Klasse Raumschiff, welches getroffen wurde.
	 */
	private void treffer(Raumschiff ziel) {
		System.out.println(ziel.getName() + " wurde getroffen!");
		ziel.setSchildeInProzent(ziel.getSchildeInProzent() - 50);
		if (ziel.getSchildeInProzent() <= 0) {
			ziel.setHuelleInProzent(ziel.getHuelleInProzent()- 50);
			ziel.setEnergieInProzent(getEnergieInProzent() - 50);
			if (ziel.getHuelleInProzent() <= 0) {
				ziel.setLebenserhaltungInProzent(0);
				nachrichtAnAlle("Die Lebenserhaltungssysteme der " + ziel.getName() + " wurden zerst�rt.");
			}
		}
	}

	/**Voraussetzung: - 
	 * (Effekt.:) Die �bergebene Nachricht wird dem broadcastKommunikator hinzugef�gt
	 * @param message Nachricht in Form eines Strings, welche im Broadcastkommunikator festgehalten werden soll.
	 */
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}

	/**Voraussetzung: - 
	 * (Effekt.:) Gibt die ArrayList broadcastKommunikator zur�ck.
	 * @return broadcastKommunikator der alle Nachrichten an alle enth�lt, wird zur�ckgegeben
	 */
	public static ArrayList<String> logbuchAusgeben() {
		return broadcastKommunikator;
	}

	/**Voraussetzung:
	 * (Effekt.:) Gibt es keine Ladung Photonentorpedo auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden! in der Konsole ausgegeben 
	 * und die Nachricht an alle -=*Click*=- ausgegeben. Ist die Anzahl der einzusetzenden Photonentorpedos gr��er als die Menge der tats�chlich Vorhandenen, 
	 * so werden alle vorhandenen Photonentorpedos eingesetzt. Ansonsten wird die Ladungsmenge Photonentorpedo �ber die Setter Methode vermindert 
	 * und die Anzahl der Photonentorpedo im Raumschiff erh�ht. Konnten Photonentorpedos eingesetzt werden, 
	 * so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben. [X] durch die Anzahl ersetzen.
	 * @param anzahlTorpedos Anzahl der geladenen Photonentorpedos des Raumschiffs 
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean torpedosAnBoard = false;
		for (Ladung tmp : this.ladungsverzeichnis) {
			if (tmp.getBezeichnung() == "Photonentorpedo") {
				torpedosAnBoard = true;
				int x;
				if (tmp.getMenge() < anzahlTorpedos) {
					x = tmp.getMenge();
					setTorpedoAnzahl(this.torpedoAnzahl + x);
					tmp.setMenge(0);
				} else {
					x = anzahlTorpedos;
					setTorpedoAnzahl(x);
					tmp.setMenge(tmp.getMenge() - anzahlTorpedos);
				}
				System.out.println(x + " Photonentorpedo(s) eingesetzt");
			}
		}
		if (torpedosAnBoard == false) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**Voraussetzung:
	 * (Effekt.:) Die Methode entscheidet anhand der �bergebenen Parameter, welche Schiffsstrukturen repariert werden sollen.
	 * Es wird eine Zufallszahl zwischen 0 - 100 erzeugt, welche f�r die Berechnung der Reparatur ben�tigt wird.
	 * Ist die �bergebene Anzahl von Androiden gr��er als die vorhandene Anzahl von Androiden im Raumschiff, dann wird die vorhandene Anzahl von Androiden eingesetzt.
	 * F�r die prozentuale Berechnung der reparierten Schiffsstrukturen wird eine Zufallszahl mit der Anzahl der eingesetzten Androiden multipliziert und durch die Anzahl der zu reparierenden Schiffsstrukturen geteit.
	 * @param schild boolean, der angibt, ob die Schilde repariert werden sollen
	 * @param huelle boolean, der angibt, ob die H�lle repariert werden sollen
	 * @param energie boolean, der angibt, ob die H�lle repariert werden sollen
	 * @param anzahlDroiden Anzahl der eingesetzen Androiden
	 */
	public void reparaturDurchfuehren(boolean schild, boolean huelle, boolean energie, int anzahlDroiden) {
		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}
		int strukturen = 0;
		if (schild == true) {
			strukturen++;
		}
		if (huelle == true) {
			strukturen++;
		}
		if (energie == true) {
			strukturen++;
		}
		int reparatur = ((int) (Math.random() * 100)) * anzahlDroiden / strukturen;

		if (schild == true) {
			this.schildeInProzent += reparatur;
		}
		if (huelle == true) {
			this.huelleInProzent += reparatur;
		}
		if (energie == true) {
			this.energieInProzent += reparatur;
		}
	}

	/**Voraussetzung: -
	 * (Effekt.:) Alle Zust�nde (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben.
	 */
	public void zustandRaumschiff() {
		System.out.println("\nName des Raumschiffs: " + this.name);
		System.out.println("Energie in Prozent: " + this.energieInProzent);
		System.out.println("Schilde in Prozent: " + this.schildeInProzent);
		System.out.println("H�lle in Prozent: " + this.huelleInProzent);
		System.out.println("Lebenserhaltung in Prozent: " + this.lebenserhaltungInProzent);
		System.out.println("Anzahl der Reparaturandroiden: " + this.androidenAnzahl);
		System.out.println("Anzahl der Torpedos: " + this.torpedoAnzahl + "\n");
	}

	/**Voraussetzung: -
	 * (Effekt.:) Alle Ladungen eines Raumschiffes auf der Konsole mit Ladungsbezeichnung und Menge ausgeben.
	 */
	public void ausgabeLadungsverzeichnis() {
		for (Ladung tmp : ladungsverzeichnis) {
			System.out.println(tmp.getBezeichnung() + ": " + tmp.getMenge());
		}
		//System.out.println(ladungsverzeichnis); geht auch
		System.out.println("\n");
	}

	/**Voraussetzung: -
	 * (Effekt.:) Wenn die Menge einer Ladung 0 ist, dann wird das Objekt Ladung aus der Liste entfernt.
	 */
	public void aufraeumen() {
		for (int i = ladungsverzeichnis.size() - 1; i >= 0; i--) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}

}