/**
 * 
 * @author Anne Wittneben
 *
 */

public class Ladung {

	// Attribute
	private String bezeichnung;
	private int menge;
	
	// Konstruktoren
	
	public Ladung() {
	}

	/**Vollparametrisierter Konstruktor
	 * 
	 * @param bezeichnung Bezeichnung der Ladung
	 * @param menge Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	// Verwaltungsmethoden
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return this.menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	// toString
	@Override
	public String toString() {
		return "Ladung " + bezeichnung + ": " + menge;
	}
}
