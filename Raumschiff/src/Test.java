
public class Test {

	public static void main(String[] args) {

		// Raumschiffe erschaffen
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 100, 100, 5, "Ni'Var");

		// Ladungen erschaffen
		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung sonde = new Ladung("Forschungssonde", 35);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		Ladung torpedo = new Ladung("Photonentorpedo", 3);

		// Raumschiffe beladen
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);

		romulaner.addLadung(borgSchrott);
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(plasmaWaffe);

		vulkanier.addLadung(torpedo);
		vulkanier.addLadung(sonde);

		// Methoden ausf�hren
		klingonen.photonentorpedosSchiessen(romulaner);
		romulaner.phaserKanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ausgabeLadungsverzeichnis();
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(5);
		vulkanier.aufraeumen();
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ausgabeLadungsverzeichnis();
		romulaner.zustandRaumschiff();
		romulaner.ausgabeLadungsverzeichnis();
		vulkanier.zustandRaumschiff();
		vulkanier.ausgabeLadungsverzeichnis();
		for (String tmp : Raumschiff.broadcastKommunikator) {
			System.out.println(tmp);
		}
	}

}
