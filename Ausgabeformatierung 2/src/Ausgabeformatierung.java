
public class Ausgabeformatierung {

	public static void main(String[] args) {
//		Aufgabe 1
		
		System.out.printf("%6s" , "**");
		System.out.print("\n");
		System.out.printf("%2s" , "*");
		System.out.printf("%8s" , "*\n");
		System.out.printf("%2s" , "*");
		System.out.printf("%8s" , "*\n");
		System.out.printf("%6s" , "**");

		
//		Aufgabe 2
		
		System.out.println("\n\n");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("= ");
		System.out.printf("%-19s", "1");
		System.out.printf("= ");
		System.out.printf("%4s", "1\n");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("= ");
		System.out.printf("%-19s", "1 * 2");
		System.out.printf("= ");
		System.out.printf("%4s", "2\n");
		
		System.out.printf("%-5s", "3!");
		System.out.printf("= ");
		System.out.printf("%-19s", "1 * 2 * 3");
		System.out.printf("= ");
		System.out.printf("%4s", "6\n");
		
		System.out.printf("%-5s", "4!");
		System.out.printf("= ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4");
		System.out.printf("= ");
		System.out.printf("%4s", "24\n");
	
		System.out.printf("%-5s", "5!");
		System.out.printf("= ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("= ");
		System.out.printf("%4s", "120\n");
		
//		Aufgabe 3
		
		System.out.println("\n\n");
		
		
		String s1 = "Fahrenheit";
		String s2 = "Celsius";		
				
		double f1 = -20;
		double f2 = -10;
		double f3 = 0;
		double f4 = 20;
		double f5 = 30;
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		System.out.printf( "%-12s" , s1 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10s\n" , s2 ); 
		
		System.out.println("-----------------------");
		
		System.out.printf( "%-12.2f" , f1 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c1 ); 
		
		System.out.printf( "%-12.2f" , f2 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c2 ); 
		
		System.out.printf( "%+-12.2f" , f3 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c3 ); 
		
		System.out.printf( "%+-12.2f" , f4 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c4 ); 
		
		System.out.printf( "%+-12.2f" , f5 ); 
		System.out.printf( "|"); 
		System.out.printf( "%10.2f\n" , c5 ); 
		
		
	}

}
