﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float ticketPreis;
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       byte anzahlTickets;

       
       // Abfrage Ticketpreis
       System.out.println("Ticketpreis (Euro): ");
       ticketPreis = tastatur.nextFloat();
       
       // Abfrage Ticketanzahl
       System.out.println("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextByte();
       
       // Berechnung und Ausgabe des zu zahlenden Betrags
       zuZahlenderBetrag = anzahlTickets * ticketPreis;
       
           
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f EURO\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }           
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05; 	         
           }

       }


       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       tastatur.close();
    }
}

	/*	Ich habe den Datentyp "byte" für die Anzahl der Tickets gewählt, da nur ganze Tickets gekauft werden können 
	 *  und definitiv nicht mehr als 127 Tickets gekauft werden.
	 *  Bei der Berechnung des zu zahlenden Betrags wird die Anzahl der Tickets (anzahlTickets) mit dem Preis eines Einzelnen Tickets (ticketPreis) 
	 *  multipliziert und in der Variable zuZahlenderBetrag gespeichert.
	 */